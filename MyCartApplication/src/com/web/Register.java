package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Dao.CustomerDao;
import com.dto.Customer;

/**
 * Servlet implementation class Register
 */
@WebServlet("/Register")
public class Register extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
	 	int custId = Integer.parseInt(request.getParameter("CustId"));
		String custName = request.getParameter("CustName");
		String mobile = request.getParameter("mobile");
		String emailId = request.getParameter("emailId");
		String loginId = request.getParameter("LoginId");
		String password = request.getParameter("password");
		
		Customer customer = new Customer(custId, custName, mobile, emailId, loginId, password);
		
		CustomerDao customerDao = new CustomerDao();
		int result = customerDao.registerCustomer(customer);
		
		out.print("<body bgcolor='lightyellow' text='green'>");
		out.print("<center>");
		if (result > 0) {
			out.print("<h1>Customer Registration Success</h1>");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("Login.html");
			requestDispatcher.include(request, response);
		} else {
			out.print("<h1 style='color:red'>Customer Registration Failed...</h1>");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("Register.html");
			requestDispatcher.include(request, response);
		}

		out.print("</center></body>");
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
