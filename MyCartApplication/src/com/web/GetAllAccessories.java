package com.web;

import java.io.IOException; 
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.Dao.CustomerDao;
import com.dto.Accessories;

/**
 * Servlet implementation class GetAllAccessories
 */
@WebServlet("/GetAllAccessories")
public class GetAllAccessories extends HttpServlet {

protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
			
		CustomerDao customerDao = new CustomerDao();
		List<Accessories> accessoriesList = customerDao.getAllAccessories();
			
		out.print("<body bgcolor='lightyellow' text='green'>");
				 
			
		if (accessoriesList != null) {	
			
			//Storing EmployeeList into the Request Object
			request.setAttribute("accessoriesList", accessoriesList);
			
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("GetAllAccessories.jsp");
			requestDispatcher.forward(request, response);
				
		} else {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("CustomerHomePage.jsp");
			requestDispatcher.include(request, response);
				
			out.print("<h1 style='color:red'>Unable to Fetch Accessories list</h1>");
		}
		out.print("</body>");
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
