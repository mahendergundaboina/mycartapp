package com.Dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.Db.DbConnection;
import com.dto.Accessories;
import com.dto.CartItems;
import com.dto.Customer;

public class CustomerDao {
public Customer custLogin(String loginId, String password) {
		
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		String loginQuery = "Select * from customers where loginId=? and password=?";
		
		try {
			preparedStatement = connection.prepareStatement(loginQuery);
			preparedStatement.setString(1, loginId);
			preparedStatement.setString(2, password);
			resultSet = preparedStatement.executeQuery();
			
			if (resultSet.next()) {
				
				Customer customer = new Customer();
				
				customer.setCustId(resultSet.getInt(1));
				customer.setCustName(resultSet.getString(2));
				customer.setMobile(resultSet.getString(3));
				customer.setEmailId(resultSet.getString(4));
				customer.setLoginId(resultSet.getString(5));
				customer.setPassword(resultSet.getString(6));
				customer.setStatus(resultSet.getString(7));

				return customer;
				
			} 
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (connection != null) {
					resultSet.close();
					preparedStatement.close();
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		return null;
	}

public int registerCustomer(Customer customer) {
	Connection connection = DbConnection.getConnection();
	PreparedStatement preparedStatement = null;
	
	String registerQuery = "insert into customers(custid,custname,mobile,emailid,loginid,password) values (?, ?, ?, ?, ?, ?)";
	
	try {
		preparedStatement = connection.prepareStatement(registerQuery);
		
		preparedStatement.setInt(1, customer.getCustId());
		preparedStatement.setString(2, customer.getCustName() );
		preparedStatement.setString(3, customer.getMobile());
		preparedStatement.setString(4, customer.getEmailId());
		preparedStatement.setString(5, customer.getLoginId());
		preparedStatement.setString(6, customer.getPassword());
		
		return preparedStatement.executeUpdate();			
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {
		try {
			if (connection != null) {
				preparedStatement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	return 0;
}

	public List<Accessories> getAllAccessories() {
		
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		String loginQuery = "Select * from accessories";
		List<Accessories> accessoriesList = new ArrayList<Accessories>();
		
		try {
			preparedStatement = connection.prepareStatement(loginQuery);
			resultSet = preparedStatement.executeQuery();
			
			if (resultSet != null) {
				
				while (resultSet.next()) {
					
					Accessories accessories = new Accessories();
					
					accessories.setProdtId(resultSet.getInt(1));
					accessories.setProdtName(resultSet.getString(2));
					accessories.setProdtType(resultSet.getString(3));
					accessories.setManufacture(resultSet.getString(4));
					accessories.setProdtPrice(resultSet.getDouble(5));

					accessoriesList.add(accessories);
				}
				
				return accessoriesList;
			} 
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (connection != null) {
					resultSet.close();
					preparedStatement.close();
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	
	return null;
}
	
public Accessories getProductById(int prodtId) {
		
		Connection connection = DbConnection.getConnection();
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;

	    String query = "SELECT * FROM accessories WHERE prodtId = ?";

	    try {
	        preparedStatement = connection.prepareStatement(query);
	        preparedStatement.setInt(1, prodtId);
	        resultSet = preparedStatement.executeQuery();

	        if (resultSet.next()) {
	            Accessories accessories = new Accessories();
	            accessories.setProdtId(resultSet.getInt("prodtId"));
	            accessories.setProdtName(resultSet.getString("prodtName"));
	            accessories.setProdtType(resultSet.getString("prodtType"));
	            accessories.setManufacture(resultSet.getString("manufacture"));
	            accessories.setProdtPrice(resultSet.getDouble("prodtprice"));
	            return accessories;
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    } finally {
	        try {
	            if (resultSet != null) {
	                resultSet.close();
	            }
	            if (preparedStatement != null) {
	                preparedStatement.close();
	            }
	            if (connection != null) {
	                connection.close();
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }
	    return null; 
	}

	public int addToCart(Accessories accessories, String loginId) {
		 	Connection connection = DbConnection.getConnection();
		    PreparedStatement preparedStatement = null;

		    String cartQuery = "INSERT INTO cart(prodtId, prodtName, prodtType, manufacture, ProdtPrice, loginId) VALUES (?, ?, ?, ?, ?, ?)";

		    try {
		        preparedStatement = connection.prepareStatement(cartQuery);
		        preparedStatement.setInt(1, accessories.getProdtId());
		        preparedStatement.setString(2, accessories.getProdtName());
		        preparedStatement.setString(3, accessories.getProdtType());
		        preparedStatement.setString(4, accessories.getManufacture());
		        preparedStatement.setDouble(5, accessories.getProdtPrice());
		        preparedStatement.setString(6, loginId);

		        return preparedStatement.executeUpdate();

		    } catch (SQLException e) {
		        e.printStackTrace();
		    } finally {
		        try {
		            if (connection != null) {
		                preparedStatement.close();
		                connection.close();
		            }
		        } catch (SQLException e) {
		            e.printStackTrace();
		        }
		    }

		return 0;
	}




	public CartItems getCartItemsById(String loginId) {
		Connection connection = DbConnection.getConnection();
	    PreparedStatement preparedStatement = null;
	    ResultSet resultSet = null;
	    

	    String query = "SELECT * FROM cart WHERE loginId = ?";

	    try {
	        
	        preparedStatement = connection.prepareStatement(query);
	        preparedStatement.setString(1, loginId);
	        resultSet = preparedStatement.executeQuery();

	        if (resultSet.next()) {
	        	
	            CartItems cartItems = new CartItems();
	            
	            cartItems.setProdtId(resultSet.getInt(1));
	            cartItems.setProdtName(resultSet.getString(2));
	            cartItems.setProdtType(resultSet.getString(3));
	            cartItems.setManufacture(resultSet.getString(4));
	            cartItems.setProdtPrice(resultSet.getDouble(5));
	            cartItems.setLoginId(resultSet.getString(6));
	            
	            return cartItems;
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    } finally {
	        try { 
	            if (connection != null){
	            	resultSet.close();
	            	preparedStatement.close();
	            	connection.close();
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }
		return null;

	    
	}

	
	}

	
	



