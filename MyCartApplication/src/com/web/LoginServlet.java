package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.Dao.CustomerDao;
import com.dto.Customer;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession(true);
		session.setAttribute("loginId", loginId);

		out.print("<html>");
		out.print("<body >");
		if (loginId.equalsIgnoreCase("Admin") && password.equals("admin")) {
			// Calling AdminHomePage (Servlet)
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("AdminHomePage.jsp");
			requestDispatcher.forward(request, response);

		}else if(loginId.equalsIgnoreCase("Cust") && password.equals("Cust")){
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("CustomerHomePage.jsp");
			requestDispatcher.forward(request, response);
		}
		else{
			CustomerDao customerDao = new CustomerDao();
			Customer customer = customerDao.custLogin(loginId, password);

			if (customer != null) {
				
				//Storing customer Object Under Session
				session.setAttribute("customer", customer);
							
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("CustomerHomePage.jsp");
				requestDispatcher.forward(request, response);

			} else {
				out.print("<h1 style='color:red'>Invalid Credentials</h1>");
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("Login.html");
				requestDispatcher.include(request, response);
			}
		}
		out.print("</body>");
		out.print("</html>");
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
