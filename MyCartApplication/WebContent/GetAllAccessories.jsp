<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<!-- Using the JSTL Core Tag -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>GetAllAccessories</title>
</head>
<body>
	
	<jsp:include page="CustomerHomePage.jsp" />
	<br />

	<table border='2' align='center'>

		<tr>
			<th>Product ID</th>
			<th>Product Name</th>
			<th>Product Type</th>
			<th>Manufacture</th>
			<th>Product Price</th>
			<th>Purchase</th>
		</tr>

		<c:forEach var="accessories" items="${accessoriesList}">		
		<tr>
			<td> ${ accessories.prodtId   } </td>
			<td> ${ accessories.prodtName } </td>
			<td> ${ accessories.prodtType  } </td>
			<td> ${ accessories.manufacture  } </td>
		 	<td> ${ accessories.prodtPrice } </td>
			<td> <a href='AddToCart?prodtId=${accessories.prodtId}'>  Add To Cart  </a> </td>
		</tr>		
		</c:forEach>

	</table>

</body>
</html>
