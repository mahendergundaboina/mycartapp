package com.dto;

public class Customer {

	private int custId;
	private String custName;
	private String mobile;
	private String emailId;
	private String loginId;
	private String password;
	private String Status;
	
	public Customer() {
		super();
	}

	public Customer(int custId, String custName, String mobile, String emailId, String loginId, String password,
			String status) {
		super();
		this.custId = custId;
		this.custName = custName;
		this.mobile = mobile;
		this.emailId = emailId;
		this.loginId = loginId;
		this.password = password;
		Status = status;
	}
	public Customer (int custId, String custName, String mobile, String emailId, String loginId, String password) {
		super();
		this.custId = custId;
		this.custName = custName;
		this.mobile = mobile;
		this.emailId = emailId;
		this.loginId = loginId;
		this.password = password;
		
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	@Override
	public String toString() {
		return "Customer [custId=" + custId + ", custName=" + custName + ", mobile=" + mobile + ", emailId=" + emailId
				+ ", loginId=" + loginId + ", password=" + password + ", Status=" + Status + "]";
	}
	
	
}
