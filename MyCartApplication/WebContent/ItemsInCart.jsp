<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Items In Cart</title>
</head>
<body>

	<jsp:include page="CustomerHomePage.jsp" />
	<br />

	<table border='2' align='center'>
		<tr>
			<th>Product ID</th>
			<th>Product Name</th>
			<th>Product Type</th>
			<th>Manufacture</th>
			<th>Product Price</th>
			<th>Login-Id</th>
		</tr>

		<c:forEach var="cartItems" items="${cartitemsList}">		
		<tr>
			<td>${cartItems.prodtId}</td>
			<td>${cartItems.prodtName}</td>
			<td>${cartItems.prodtType}</td>
			<td>${cartItems.manufacture}</td>
			<td>${cartItems.prodtPrice}</td>
			<td>${cartItems.loginId}</td>
		</tr>
		</c:forEach>

	</table>
</body>
</html>
