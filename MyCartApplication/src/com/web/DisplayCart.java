package com.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.Dao.CustomerDao;
import com.dto.Accessories;
import com.dto.CartItems;

@WebServlet("/DisplayCart")
public class DisplayCart extends HttpServlet {

protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		HttpSession session = request.getSession();
        String loginId = (String) session.getAttribute("loginId");
        
		CustomerDao customerDao = new CustomerDao();
		CartItems cartItems = customerDao.getCartItemsById(loginId);
			
		out.print("<body bgcolor='lightyellow' text='green'>");
				 
			
		if (cartItems != null) {	
			
			//Storing EmployeeList into the Request Object
			request.setAttribute("cartItems", cartItems);
			
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("ItemsInCart.jsp");
			requestDispatcher.forward(request, response);
				
		} else {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("CustomerHomePage.jsp");
			requestDispatcher.include(request, response);
				
			out.print("<h1 style='color:red'>Unable to Fetch cartItems list</h1>");
		}
		out.print("</body>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
