package com.dto;

public class Accessories {
private int prodtId;
private String prodtName;
private String prodtType;
private String manufacture;
private Double prodtPrice;
public Accessories() {
	super();
	// TODO Auto-generated constructor stub
}
public Accessories(int prodtId, String prodtName, String prodtType, String manufacture, Double prodtPrice) {
	super();
	this.prodtId = prodtId;
	this.prodtName = prodtName;
	this.prodtType = prodtType;
	this.manufacture = manufacture;
	this.prodtPrice = prodtPrice;
}
public int getProdtId() {
	return prodtId;
}
public void setProdtId(int prodtId) {
	this.prodtId = prodtId;
}
public String getProdtName() {
	return prodtName;
}
public void setProdtName(String prodtName) {
	this.prodtName = prodtName;
}
public String getProdtType() {
	return prodtType;
}
public void setProdtType(String prodtType) {
	this.prodtType = prodtType;
}
public String getManufacture() {
	return manufacture;
}
public void setManufacture(String manufacture) {
	this.manufacture = manufacture;
}
public Double getProdtPrice() {
	return prodtPrice;
}
public void setProdtPrice(Double prodtPrice) {
	this.prodtPrice = prodtPrice;
}
@Override
public String toString() {
	return "Accessories [prodtId=" + prodtId + ", prodtName=" + prodtName + ", prodtType=" + prodtType
			+ ", manufacture=" + manufacture + ", prodtPrice=" + prodtPrice + "]";
}



}
