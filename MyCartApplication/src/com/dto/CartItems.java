package com.dto;

public class CartItems {
    private int prodtId;
    private String prodtName;
    private String prodtType;
    private String manufacture;
    private double prodtPrice;
    private String loginId;
    
    public CartItems() {
		super();
		// TODO Auto-generated constructor stub
	}
    
	public CartItems(int prodtId, String prodtName, String prodtType, String manufacture, double prodtPrice,
			String loginId) {
		super();
		this.prodtId = prodtId;
		this.prodtName = prodtName;
		this.prodtType = prodtType;
		this.manufacture = manufacture;
		this.prodtPrice = prodtPrice;
		this.loginId = loginId;
	}
	
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public int getProdtId() {
		return prodtId;
	}
	public void setProdtId(int prodtId) {
		this.prodtId = prodtId;
	}
	public String getProdtName() {
		return prodtName;
	}
	public void setProdtName(String prodtName) {
		this.prodtName = prodtName;
	}
	public String getProdtType() {
		return prodtType;
	}
	public void setProdtType(String prodtType) {
		this.prodtType = prodtType;
	}
	public String getManufacture() {
		return manufacture;
	}
	public void setManufacture(String manufacture) {
		this.manufacture = manufacture;
	}
	public double getProdtPrice() {
		return prodtPrice;
	}
	public void setProdtPrice(double prodtPrice) {
		this.prodtPrice = prodtPrice;
	}
	@Override
	public String toString() {
		return "CartItems [prodtId=" + prodtId + ", prodtName=" + prodtName + ", prodtType=" + prodtType
				+ ", manufacture=" + manufacture + ", prodtPrice=" + prodtPrice + "]";
	}
    
    
}
