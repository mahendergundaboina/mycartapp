package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.Dao.CustomerDao;
import com.dto.Accessories;

@WebServlet("/AddToCart")
public class AddToCart extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	response.setContentType("text/html");
    	PrintWriter out = response.getWriter();
    	
    	HttpSession session = request.getSession();
    	int prodtId = Integer.parseInt(request.getParameter("prodtId"));

    	String loginId = (String) session.getAttribute("loginId");
    		
    	CustomerDao customerDao = new CustomerDao();
    	Accessories accessories = customerDao.getProductById(prodtId);

    	int result = customerDao.addToCart(accessories,loginId);
    		
    	if (result > 0) {
    		request.getRequestDispatcher("GetAllAccessories").forward(request, response);
    	} else {
    		request.getRequestDispatcher("CustomerHomePage.jsp").include(request, response);
    		out.print("<br/>");
    		out.print("<h2 style='color:red;'>Unable to get Record!!!</h2>");
    	}
    		
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
	}

}
